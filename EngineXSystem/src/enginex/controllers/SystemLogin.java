package enginex.controllers;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import enginex.entities.RolesFactory;
import enginex.entities.Rolls;

@SuppressWarnings("serial")
public class SystemLogin extends HttpServlet {
	
	 static DatastoreService datastore=DatastoreServiceFactory.getDatastoreService();
	
	
	
	@Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
              throws IOException {
     UserService userService = UserServiceFactory.getUserService();
     User user = userService.getCurrentUser();
     
        if (user != null) {
        	//String sessionId;
        	
        	Rolls temp = isSystemUser(user);
        	String state = null;
        	if(temp==null){
        		state="Check Again";
        		resp.setContentType("text/plain");
                resp.getWriter().println("Hello, Your ID is invalid for this section. "+ state);
                resp.sendRedirect(userService.createLoginURL(req.getRequestURI()));
                
        	}else{
        		
        		/*HttpSession httpSession = req.getSession(true);
        		httpSession.setAttribute("Logger",Logger.getLogger(temp.getUsername()));
        		System.out.println(httpSession.toString());*/
        		
        		 resp.setContentType("text/plain");
                 resp.getWriter().println("Hello, Your Role is " +temp);
                 System.out.println("http://localhost:8888/"+temp.getLink());
                 resp.sendRedirect("http://localhost:8888/"+temp.getLink());
                 //resp.sendRedirect("http://2-dot-venginext.appspot.com/"+temp.getLink());
                 
        	}
        	
            
        } else {
            resp.sendRedirect(userService.createLoginURL(req.getRequestURI()));
        }
	}
	
	public static Rolls isSystemUser(User user){
		
		//String userEmailId= user.getNickname();
		
		
		
		Filter filter = new FilterPredicate("UserName",FilterOperator.EQUAL,user.getNickname());

		Query q = new Query("SystemUser").setFilter(filter);
						
		PreparedQuery pq = datastore.prepare(q);

		Entity result = pq.asSingleEntity();//if there are more than one result you MUST use a List
		try{
			
		String role= result.getProperty("Roles").toString();
		
		RolesFactory rolesFactory=new RolesFactory();
		Rolls roles=null;
		
		roles = rolesFactory.makeRole(role, result.getProperty("UserName").toString(),  "pass");
		
		System.out.println(result.getProperty("UserName").toString());
		
		return roles;
		}catch(NullPointerException e){
			return null;
		}
			
	}
	
	
}
