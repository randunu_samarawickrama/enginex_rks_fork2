package enginex.entities.product.deprecated;

import java.util.List;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;

import enginex.entities.EntityManager;
import enginex.entities.product.Product;

public class ProductDAODeprecated {
	private List<Product> producList = null;
	private Product product = new Product();
	DatastoreService xt = DatastoreServiceFactory.getDatastoreService();
	EntityManager man1 = new EntityManager();
	
	
	public void addProducts(List<Product> listOfProducts){
		man1.createProducts(listOfProducts);
	}
	
	public List<Product> listProducts(){
		PreparedQuery pq =  man1.getAllEntities("Product");
		product = new Product();
		for (Entity result : pq.asIterable()) {
			//System.out.println(result.toString());
			product.setDescription((String) result.getProperty("Description"));
			product.setEngineCode((String) result.getProperty("EngineCode"));
			product.setPendingOrders(Integer.parseInt(result.getProperty("PendingOrders").toString()));
			product.setPendingProduction(Integer.parseInt(result.getProperty("PendingProduction").toString()));
			product.setStockLevel(Integer.parseInt(result.getProperty("StockLevel").toString()));
			
			producList.add(product);
		}
		return producList;
	}
	
	public Product getProduct(String codeName){
		PreparedQuery pq = man1.getEntity("ProductTester2","CodeName",codeName);
		product = new Product();
		for (Entity result : pq.asIterable()) {
			//System.out.println(result.toString());
			product.setDescription((String) result.getProperty("Description"));
			product.setEngineCode((String) result.getProperty("EngineCode"));
			product.setPendingOrders(Integer.parseInt(result.getProperty("PendingOrders").toString()));
			product.setPendingProduction(Integer.parseInt(result.getProperty("PendingProduction").toString()));
			product.setStockLevel(Integer.parseInt(result.getProperty("StockLevel").toString()));
		}

		return product;
		
	}


}
