package enginex.entities.product;

import java.util.ArrayList;
import java.util.List;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;

import enginex.entities.EntityManager;
import enginex.entities.product.Product;

public class ProductDAO {
	private List<Product> producList = null;
	DatastoreService xt = DatastoreServiceFactory.getDatastoreService();
	EntityManager man1 = new EntityManager();
	
	
	public void addProducts(List<Product> listOfProducts){
		man1.createProducts(listOfProducts);
	}
	
	public void updateStock(String codeName,int changeAmount){
		Product prod = new Product();

		System.out.println("updateStock() \n"+" CodeName="+codeName+ " Chamge Amount="+ changeAmount);
		int newAmount;
		prod = getProduct(codeName);
		newAmount = changeAmount + prod.getStockLevel();
		System.out.println("Current Stock Level="+prod.getStockLevel()+ " New Stock level"+ newAmount);
		prod.setStockLevel(newAmount);
		System.out.println(prod);
		man1.updateProduct(prod);
	}
	
	public List<Product> listProducts(){
		PreparedQuery pq =  man1.getAllEntities("Product");
		Product product = new Product();
		for (Entity result : pq.asIterable()) {
			product=parseEntityToProduct(result);
			producList.add(product);
		}
		return producList;
	}
	
	public Product getProduct(String codeName){
		PreparedQuery pq = man1.getEntity("Product","CodeName",codeName);
		Product product = new Product();
		for (Entity result : pq.asIterable()) {
			product = parseEntityToProduct(result);
		}

		return product;
		
	}
	
	public List<Product> getAllProducts(){
		List<Product> liPro = new ArrayList<Product>();
		Product product = new Product();
		PreparedQuery pq = man1.getAllEntities("Product");
		for (Entity result : pq.asIterable()) {
			product = parseEntityToProduct(result);
			liPro.add(product);
	}
		return liPro;
	}
	
	public Product getProductByKey(String key){
		Entity result = (Entity) man1.getEntityByKey("Product", key);
		Product product = parseEntityToProduct(result);
		return product;
	}
	
	public Product parseEntityToProduct(Entity result){
		Product product = new Product();
		product.setDescription((String) result.getProperty("Description"));
		product.setEngineCode((String) result.getProperty("EngineCode"));
		product.setCodeName((String)result.getProperty("CodeName"));
		product.setPendingOrders(Integer.parseInt(result.getProperty("PendingOrders").toString()));
		product.setPendingProduction(Integer.parseInt(result.getProperty("PendingProduction").toString()));
		product.setStockLevel(Integer.parseInt(result.getProperty("StockLevel").toString()));
		
		return product;
	}


		
}
