package enginex.entities;

public class Admin extends Rolls {
	
	private String link="authentication/createrolls.jsp";
	public Admin(){
		
	}
	
	public String createSystemUser(){
		return "admin";
	}
	
	@Override
	public String toString() {
		
		return "admin";
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
